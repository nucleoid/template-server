const fs = require("fs");
var directory;
const mustache = /{{.+}}/g;

function scan(req, res, template) {
  fs.readFile(`${directory}${template}`, "utf8", function(error, data) {
    if (error) {
      res.status(500).send("Template doesn't exist.");
    }

    let template = data.replace(mustache, function(match) {
      let file = match
        .substring(2)
        .slice(0, -2)
        .trim();

      try {
        return fs.readFileSync(`${directory}${req.path}${file}`, "utf8").trim();
      } catch (error) {
        return match;
      }
    });

    res.type("text/html").send(template);
  });
}

function register(app, config) {
  directory = config.directory;

  app.get("*.*", (req, res) =>
    res.sendFile(req.url.slice(1), { root: directory })
  );

  for (let url in config.templates)
    app.get(url, (req, res) => scan(req, res, config.templates[url]));
}

module.exports.register = register;
