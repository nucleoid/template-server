const fs = require("fs");

const https = require("https");
const express = require("express");
const template = require("./template");
const directory = process.cwd();

const app = express();

const config = JSON.parse(fs.readFileSync("template.json", "utf8"));
config.directory = directory;

if (config.before) {
  let before = require(`${directory}/${config.before}`);
  before(app);
}

template.register(app, config);

if (config.after) {
  let after = require(`${directory}/${config.after}`);
  after(app);
}

if (config.cert && config.key) {
  let cert = fs.readFileSync(config.cert, "utf8");
  let key = fs.readFileSync(config.key, "utf8");

  let port = config.port ? config.port : 443;
  https.createServer({ key, cert }, app).listen(port);

  if (port === 443) {
    var http = express();

    http.get("*", function(req, res) {
      res.redirect(`https://${req.headers.host}${req.url}`);
    });

    http.listen(80);
  }
} else {
  let port = config.port ? config.port : 80;
  app.listen(port);
}
